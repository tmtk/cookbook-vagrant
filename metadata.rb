maintainer  "Tomotaka Sakuma"
license     "MIT"
description "Install Vagrant"
version     "0.0.1"

supports :mac_os_x
supports :centos

depends :virtualbox
