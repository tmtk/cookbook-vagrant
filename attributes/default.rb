default[:vagrant][:version] = '1.3.5'
default[:vagrant][:"1.2.4"][:mac_os_x][:source] = "http://files.vagrantup.com/packages/0219bb87725aac28a97c0e924c310cc97831fd9d/Vagrant-1.2.4.dmg"
default[:vagrant][:"1.2.7"][:mac_os_x][:source] = "http://files.vagrantup.com/packages/7ec0ee1d00a916f80b109a298bab08e391945243/Vagrant-1.2.7.dmg"
default[:vagrant][:"1.3.1"][:mac_os_x][:source] = "http://files.vagrantup.com/packages/b12c7e8814171c1295ef82416ffe51e8a168a244/Vagrant-1.3.1.dmg"
default[:vagrant][:"1.3.5"][:mac_os_x][:source] = "http://files.vagrantup.com/packages/a40522f5fabccb9ddabad03d836e120ff5d14093/Vagrant-1.3.5.dmg"
default[:vagrant][:"1.2.4"][:centos][:source] = "http://files.vagrantup.com/packages/0219bb87725aac28a97c0e924c310cc97831fd9d/vagrant_1.2.4_x86_64.rpm"
default[:vagrant][:"1.2.7"][:centos][:source] = "http://files.vagrantup.com/packages/7ec0ee1d00a916f80b109a298bab08e391945243/vagrant_1.2.7_x86_64.rpm"
default[:vagrant][:"1.3.1"][:centos][:source] = "http://files.vagrantup.com/packages/b12c7e8814171c1295ef82416ffe51e8a168a244/vagrant_1.3.1_x86_64.rpm"
default[:vagrant][:"1.3.5"][:centos][:source] = "http://files.vagrantup.com/packages/a40522f5fabccb9ddabad03d836e120ff5d14093/vagrant_1.3.5_x86_64.rpm"

default[:vagrant][:source] = default.vagrant[default.vagrant.version][node.platform].source
default[:vagrant][:archive] = "#{ENV['HOME']}/Downloads/#{File.basename default.vagrant.source}"
default[:vagrant][:mount] = "/Volumes/Vagrant"

default[:vagrant][:home] = "/Applications/Vagrant"
default[:vagrant][:bin_dir] = "#{default.vagrant.home}/bin"
