case node.platform
when "mac_os_x"

  remote_file node.vagrant.archive do
    action :create_if_missing
    source node.vagrant.source
  end

  script "install vagrant" do
    interpreter "bash"
    code <<-EOF
    hdiutil mount #{node.vagrant.archive}
    installer -pkg '#{node.vagrant.mount}/Vagrant.pkg' -target /
    umount '#{node.vagrant.mount}/'
    EOF
  end

when "centos"

  tmppath = "/tmp/vagrant_#{node.vagrant.version}_x86_64.rpm"

  remote_file tmppath do
    action :create_if_missing
    source node.vagrant.source
  end

  package "vagrant" do
    action :install
    source tmppath
    provider Chef::Provider::Package::Rpm
  end

end  
